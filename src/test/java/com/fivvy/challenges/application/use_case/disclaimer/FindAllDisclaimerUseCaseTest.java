package com.fivvy.challenges.application.use_case.disclaimer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fivvy.challenges.domain.model.disclaimer.Disclaimer;
import com.fivvy.challenges.domain.model.disclaimer.driven_port.DisclaimerRepositoryPort;
import com.fivvy.challenges.domain.model.page.Page;
import com.fivvy.challenges.domain.model.page.Pageable;
import com.fivvy.challenges.mock.disclaimer.DisclaimerMock;
import java.util.ArrayList;
import java.util.Collections;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class FindAllDisclaimerUseCaseTest {

    @Mock
    private DisclaimerRepositoryPort disclaimerRepositoryPort;

    @InjectMocks
    private FindAllDisclaimerUseCase useCase;

    @Test
    void execute() {

        // Arrange
        var page = new Page<Disclaimer>();
        page.setCurrentPage(0);
        page.setContent(
            new ArrayList<>(Collections.singletonList(DisclaimerMock.build()))
        );

        when(disclaimerRepositoryPort.findAll(any())).thenReturn(page);

        // Act
        var response = useCase.execute(new Pageable());

        // Assert
        assertEquals(1, response.getContent().size());
        assertEquals("Name test", response.getContent().get(0).getName());
    }
}