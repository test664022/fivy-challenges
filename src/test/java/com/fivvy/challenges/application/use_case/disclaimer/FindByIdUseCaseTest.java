package com.fivvy.challenges.application.use_case.disclaimer;

import static java.util.UUID.randomUUID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fivvy.challenges.domain.model.disclaimer.driven_port.DisclaimerRepositoryPort;
import com.fivvy.challenges.mock.disclaimer.DisclaimerMock;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class FindByIdUseCaseTest {

    @Mock
    private DisclaimerRepositoryPort disclaimerRepositoryPort;

    @InjectMocks
    private FindByIdUseCase useCase;

    @Test
    void execute() {

        // Arrange
        var disclaimer = DisclaimerMock.build();

        when(disclaimerRepositoryPort.findById(any())).thenReturn(disclaimer);

        // Act
        var response = useCase.execute(randomUUID());

        // Assert
        assertEquals(disclaimer.getName(), response.getName());
        assertEquals(disclaimer.getText(), response.getText());
    }
}