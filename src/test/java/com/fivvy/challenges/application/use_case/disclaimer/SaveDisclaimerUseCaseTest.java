package com.fivvy.challenges.application.use_case.disclaimer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fivvy.challenges.domain.model.disclaimer.Disclaimer;
import com.fivvy.challenges.domain.model.disclaimer.driven_port.DisclaimerRepositoryPort;
import com.fivvy.challenges.mock.disclaimer.DisclaimerMock;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SaveDisclaimerUseCaseTest {

    @Mock
    private DisclaimerRepositoryPort disclaimerRepositoryPort;

    @InjectMocks
    private SaveDisclaimerUseCase useCase;

    @Test
    void execute() {

        // Arrange
        var disclaimer = DisclaimerMock.build();

        when(disclaimerRepositoryPort.save(any())).thenReturn(disclaimer);

        // Act
        var response = useCase.execute(new Disclaimer());

        // Assert
        assertEquals(disclaimer.getName(), response.getName());
        assertEquals(disclaimer.getText(), response.getText());
    }
}