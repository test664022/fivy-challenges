package com.fivvy.challenges.application.use_case.disclaimer;

import static java.util.UUID.randomUUID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fivvy.challenges.domain.model.disclaimer.Disclaimer;
import com.fivvy.challenges.domain.model.disclaimer.driven_port.DisclaimerRepositoryPort;
import com.fivvy.challenges.mock.disclaimer.DisclaimerMock;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UpdateDisclaimerUseCaseTest {

    @Mock
    private DisclaimerRepositoryPort disclaimerRepositoryPort;

    @InjectMocks
    private UpdateDisclaimerUseCase useCase;

    @Test
    void execute() {

        // Arrange
        var disclaimer = DisclaimerMock.build();

        when(disclaimerRepositoryPort.update(any(), any())).thenReturn(disclaimer);

        // Act
        var response = useCase.execute(randomUUID(), new Disclaimer());

        // Assert
        assertEquals(disclaimer.getName(), response.getName());
        assertEquals(disclaimer.getText(), response.getText());
    }
}