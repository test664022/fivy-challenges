package com.fivvy.challenges.application.use_case.acceptance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fivvy.challenges.domain.model.acceptance.Acceptance;
import com.fivvy.challenges.domain.model.acceptance.driven_port.AcceptanceRepositoryPort;
import com.fivvy.challenges.domain.model.page.Page;
import com.fivvy.challenges.domain.model.page.Pageable;
import com.fivvy.challenges.mock.acceptance.AcceptanceMock;
import java.util.ArrayList;
import java.util.Collections;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class FindAllAcceptanceUseCaseTest {

    @Mock
    private AcceptanceRepositoryPort acceptanceRepositoryPort;

    @InjectMocks
    private FindAllAcceptanceUseCase useCase;

    @Test
    void execute() {

        // Arrange
        var page = new Page<Acceptance>();
        page.setCurrentPage(0);
        page.setContent(
            new ArrayList<>(Collections.singletonList(AcceptanceMock.build()))
        );

        when(acceptanceRepositoryPort.findAll(any())).thenReturn(page);

        // Act
        var response = useCase.execute(new Pageable());

        // Assert
        assertEquals(1, response.getContent().size());
        assertNotNull(response.getContent().get(0).getUserId());
    }
}