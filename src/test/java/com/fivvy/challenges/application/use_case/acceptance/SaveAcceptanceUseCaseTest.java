package com.fivvy.challenges.application.use_case.acceptance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fivvy.challenges.domain.model.acceptance.driven_port.AcceptanceRepositoryPort;
import com.fivvy.challenges.domain.model.disclaimer.driven_port.DisclaimerRepositoryPort;
import com.fivvy.challenges.mock.acceptance.AcceptanceMock;
import com.fivvy.challenges.mock.disclaimer.DisclaimerMock;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SaveAcceptanceUseCaseTest {

    @Mock
    private AcceptanceRepositoryPort acceptanceRepositoryPort;

    @Mock
    private DisclaimerRepositoryPort disclaimerRepositoryPort;

    @InjectMocks
    private SaveAcceptanceUseCase useCase;

    @Test
    void execute() {

        // Arrange
        var acceptanceRequest = AcceptanceMock.build();
        acceptanceRequest.setDisclaimer(DisclaimerMock.build());

        var acceptance = AcceptanceMock.build();

        when(disclaimerRepositoryPort.findById(any()))
            .thenReturn(DisclaimerMock.build());

        when(acceptanceRepositoryPort.save(any())).thenReturn(acceptance);

        // Act
        var response = useCase.execute(acceptanceRequest);

        // Assert
        assertNotNull(response);
        assertEquals(acceptance.getUserId(), response.getUserId());
    }
}