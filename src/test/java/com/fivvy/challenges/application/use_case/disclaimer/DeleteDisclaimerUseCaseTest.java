package com.fivvy.challenges.application.use_case.disclaimer;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

import com.fivvy.challenges.domain.model.disclaimer.driven_port.DisclaimerRepositoryPort;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DeleteDisclaimerUseCaseTest {

    @Mock
    private DisclaimerRepositoryPort disclaimerRepositoryPort;

    @InjectMocks
    private DeleteDisclaimerUseCase useCase;

    @Test
    void execute() {

        // Arrange
        doNothing().when(disclaimerRepositoryPort).delete(Mockito.any());

        // Act
        useCase.execute(UUID.randomUUID());

        // Assert
        verify(disclaimerRepositoryPort).delete(Mockito.any());
    }
}