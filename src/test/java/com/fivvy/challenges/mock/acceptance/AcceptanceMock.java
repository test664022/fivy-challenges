package com.fivvy.challenges.mock.acceptance;

import static java.util.UUID.randomUUID;

import com.fivvy.challenges.domain.model.acceptance.Acceptance;

public class AcceptanceMock {

    static public Acceptance build() {

        var acceptance = new Acceptance();
        acceptance.setUserId(randomUUID());

        return acceptance;
    }
}
