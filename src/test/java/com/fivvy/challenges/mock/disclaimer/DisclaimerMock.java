package com.fivvy.challenges.mock.disclaimer;

import com.fivvy.challenges.domain.model.disclaimer.Disclaimer;

public class DisclaimerMock {

    static public Disclaimer build() {

        var disclaimer = new Disclaimer();
        disclaimer.setName("Name test");
        disclaimer.setText("Text test");

        return disclaimer;
    }
}
