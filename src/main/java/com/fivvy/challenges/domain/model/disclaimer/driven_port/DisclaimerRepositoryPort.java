package com.fivvy.challenges.domain.model.disclaimer.driven_port;

import com.fivvy.challenges.domain.model.disclaimer.Disclaimer;
import com.fivvy.challenges.domain.model.page.Page;
import com.fivvy.challenges.domain.model.page.Pageable;
import java.util.UUID;

public interface DisclaimerRepositoryPort {

    Page<Disclaimer> findAll(Pageable pageable);

    Disclaimer findById(UUID disclaimerId);

    Disclaimer save(Disclaimer disclaimer);

    Disclaimer update(UUID disclaimerId, Disclaimer disclaimer);

    void delete(UUID disclaimerId);

}
