package com.fivvy.challenges.domain.model.page;

import lombok.Data;

@Data
public class Pageable {

  private Integer page;

  private String query;

  private Integer size;

}
