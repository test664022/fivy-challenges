package com.fivvy.challenges.domain.model.acceptance.driven_port;

import com.fivvy.challenges.domain.model.acceptance.Acceptance;
import com.fivvy.challenges.domain.model.page.Page;
import com.fivvy.challenges.domain.model.page.Pageable;

public interface AcceptanceRepositoryPort {

    Page<Acceptance> findAll(Pageable pageable);

    Acceptance save(Acceptance acceptance);

}
