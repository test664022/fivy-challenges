package com.fivvy.challenges.domain.model.disclaimer;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Data;

@Data
public class Disclaimer {

  private LocalDateTime createAt;

  private UUID id;

  private String name;

  private String text;

  private LocalDateTime updateAt;

  private Integer version;

}
