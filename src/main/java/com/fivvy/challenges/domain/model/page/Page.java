package com.fivvy.challenges.domain.model.page;

import java.util.List;
import lombok.Data;

@Data
public class Page<T> {

  private List<T> content;

  private Integer currentPage;

  private Integer totalElements;

  private Integer totalPages;

}
