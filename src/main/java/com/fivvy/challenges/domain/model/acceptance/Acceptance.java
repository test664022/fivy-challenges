package com.fivvy.challenges.domain.model.acceptance;

import com.fivvy.challenges.domain.model.disclaimer.Disclaimer;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Data;

@Data
public class Acceptance {

    private LocalDateTime createAt;

    private Disclaimer disclaimer;

    private UUID id;

    private LocalDateTime updateAt;

    private UUID userId;

}
