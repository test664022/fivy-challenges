package com.fivvy.challenges.infrastructure.rest_api.controller.acceptance.v1;

import com.fivvy.challenges.application.use_case.acceptance.FindAllAcceptanceUseCase;
import com.fivvy.challenges.application.use_case.acceptance.SaveAcceptanceUseCase;
import com.fivvy.challenges.infrastructure.rest_api.dto.acceptance.AcceptanceRequestDto;
import com.fivvy.challenges.infrastructure.rest_api.dto.acceptance.AcceptanceResponseDto;
import com.fivvy.challenges.infrastructure.rest_api.dto.page.PageResponseDto;
import com.fivvy.challenges.infrastructure.rest_api.dto.page.PageableRequestDto;
import com.fivvy.challenges.infrastructure.rest_api.mapper.acceptance.AcceptanceRestApiMapper;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("api/v1/acceptances")
public class AcceptanceController {

    private FindAllAcceptanceUseCase findAllAcceptanceUseCase;
    private SaveAcceptanceUseCase saveAcceptanceUseCase;
    private AcceptanceRestApiMapper acceptanceRestApiMapper;

    @GetMapping
    public PageResponseDto<AcceptanceResponseDto> findAll(PageableRequestDto pageable) {

        return acceptanceRestApiMapper.toPageResponseDto(
            findAllAcceptanceUseCase.execute(pageable.toDomain())
        );
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public AcceptanceResponseDto save(@RequestBody AcceptanceRequestDto requestDto) {

        var acceptance = saveAcceptanceUseCase.execute(
            acceptanceRestApiMapper.toDomain(requestDto)
        );

        return acceptanceRestApiMapper.toResponseDto(acceptance);
    }

}
