package com.fivvy.challenges.infrastructure.rest_api.mapper.acceptance;

import com.fivvy.challenges.domain.model.acceptance.Acceptance;
import com.fivvy.challenges.domain.model.page.Page;
import com.fivvy.challenges.infrastructure.rest_api.dto.acceptance.AcceptanceRequestDto;
import com.fivvy.challenges.infrastructure.rest_api.dto.acceptance.AcceptanceResponseDto;
import com.fivvy.challenges.infrastructure.rest_api.dto.page.PageResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(
    componentModel = "spring",
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface AcceptanceRestApiMapper {

    @Mapping(source = "disclaimerId", target = "disclaimer.id")
    Acceptance toDomain(AcceptanceRequestDto requestDto);

    AcceptanceResponseDto toResponseDto(Acceptance acceptance);

    @Mapping(expression = "java( domainPage.getContent().size() )", target = "size")
    @Mapping(source = "currentPage", target = "page")
    PageResponseDto<AcceptanceResponseDto> toPageResponseDto(Page<Acceptance> domainPage);

}
