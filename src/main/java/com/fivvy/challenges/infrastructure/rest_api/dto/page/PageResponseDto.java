package com.fivvy.challenges.infrastructure.rest_api.dto.page;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class PageResponseDto<T> {

    private Integer size;

    private Integer totalElements;

    private Integer page;

    private Integer totalPages;

    private List<T> content = new ArrayList<>();

}
