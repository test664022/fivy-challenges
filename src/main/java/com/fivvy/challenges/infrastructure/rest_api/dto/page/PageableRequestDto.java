package com.fivvy.challenges.infrastructure.rest_api.dto.page;

import com.fivvy.challenges.domain.model.page.Pageable;
import lombok.Data;

@Data
public class PageableRequestDto {

    private Integer page = 0;

    private String search;

    private Integer size = 20;

    public Pageable toDomain() {

        var pageable = new Pageable();
        pageable.setPage(page);
        pageable.setQuery(search);
        pageable.setSize(size);

        return pageable;
    }

}
