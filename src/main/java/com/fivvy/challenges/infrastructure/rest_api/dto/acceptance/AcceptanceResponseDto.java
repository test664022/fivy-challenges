package com.fivvy.challenges.infrastructure.rest_api.dto.acceptance;

import com.fivvy.challenges.domain.model.disclaimer.Disclaimer;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Data;

@Data
public class AcceptanceResponseDto {

    private LocalDateTime createAt;

    private UUID id;

    private Disclaimer disclaimer;

    private LocalDateTime updateAt;

    private UUID userId;

}
