package com.fivvy.challenges.infrastructure.rest_api.controller.disclaimer.v1;

import com.fivvy.challenges.application.use_case.disclaimer.DeleteDisclaimerUseCase;
import com.fivvy.challenges.application.use_case.disclaimer.FindAllDisclaimerUseCase;
import com.fivvy.challenges.application.use_case.disclaimer.FindByIdUseCase;
import com.fivvy.challenges.application.use_case.disclaimer.SaveDisclaimerUseCase;
import com.fivvy.challenges.application.use_case.disclaimer.UpdateDisclaimerUseCase;
import com.fivvy.challenges.infrastructure.rest_api.dto.disclaimer.DisclaimerRequestDto;
import com.fivvy.challenges.infrastructure.rest_api.dto.disclaimer.DisclaimerResponseDto;
import com.fivvy.challenges.infrastructure.rest_api.dto.page.PageResponseDto;
import com.fivvy.challenges.infrastructure.rest_api.dto.page.PageableRequestDto;
import com.fivvy.challenges.infrastructure.rest_api.mapper.disclaimer.DisclaimerRestApiMapper;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("api/v1/disclaimers")
public class DisclaimerController {

    private DeleteDisclaimerUseCase deleteDisclaimerUseCase;
    private FindAllDisclaimerUseCase findAllDisclaimerUseCase;
    private FindByIdUseCase findByIdUseCase;
    private SaveDisclaimerUseCase saveDisclaimerUseCase;
    private UpdateDisclaimerUseCase updateDisclaimerUseCase;
    private DisclaimerRestApiMapper disclaimerRestApiMapper;

    @GetMapping
    public PageResponseDto<DisclaimerResponseDto> findAll(PageableRequestDto pageable) {

        return disclaimerRestApiMapper.toPageResponseDto(
            findAllDisclaimerUseCase.execute(pageable.toDomain())
        );
    }

    @GetMapping("/{id}")
    public DisclaimerResponseDto findById(@PathVariable UUID id) {

        return disclaimerRestApiMapper.toResponseDto(
            findByIdUseCase.execute(id)
        );
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public DisclaimerResponseDto save(@RequestBody DisclaimerRequestDto requestDto) {

        var disclaimer = saveDisclaimerUseCase.execute(
            disclaimerRestApiMapper.toDomain(requestDto)
        );

        return disclaimerRestApiMapper.toResponseDto(disclaimer);
    }

    @PutMapping("/{id}")
    public DisclaimerResponseDto update(
        @PathVariable UUID id,
        @RequestBody DisclaimerRequestDto requestDto
    ) {

        var disclaimer = updateDisclaimerUseCase.execute(
            id,
            disclaimerRestApiMapper.toDomain(requestDto)
        );

        return disclaimerRestApiMapper.toResponseDto(disclaimer);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) {

        deleteDisclaimerUseCase.execute(id);
    }

}
