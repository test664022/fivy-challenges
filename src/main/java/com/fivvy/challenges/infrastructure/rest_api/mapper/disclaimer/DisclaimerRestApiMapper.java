package com.fivvy.challenges.infrastructure.rest_api.mapper.disclaimer;

import com.fivvy.challenges.domain.model.disclaimer.Disclaimer;
import com.fivvy.challenges.domain.model.page.Page;
import com.fivvy.challenges.infrastructure.rest_api.dto.disclaimer.DisclaimerRequestDto;
import com.fivvy.challenges.infrastructure.rest_api.dto.disclaimer.DisclaimerResponseDto;
import com.fivvy.challenges.infrastructure.rest_api.dto.page.PageResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(
    componentModel = "spring",
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface DisclaimerRestApiMapper {

    Disclaimer toDomain(DisclaimerRequestDto requestDto);

    DisclaimerResponseDto toResponseDto(Disclaimer disclaimer);

    @Mapping(expression = "java( domainPage.getContent().size() )", target = "size")
    @Mapping(source = "currentPage", target = "page")
    PageResponseDto<DisclaimerResponseDto> toPageResponseDto(Page<Disclaimer> domainPage);

}
