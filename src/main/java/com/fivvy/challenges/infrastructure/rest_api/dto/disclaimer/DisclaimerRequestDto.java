package com.fivvy.challenges.infrastructure.rest_api.dto.disclaimer;

import lombok.Data;

@Data
public class DisclaimerRequestDto {

    private String name;

    private String text;

    private Integer version;

}
