package com.fivvy.challenges.infrastructure.rest_api.dto.acceptance;

import java.util.UUID;
import lombok.Data;

@Data
public class AcceptanceRequestDto {

    private UUID disclaimerId;

    private UUID userId;

}
