package com.fivvy.challenges.infrastructure.rest_api.dto.disclaimer;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Data;

@Data
public class DisclaimerResponseDto {

    private LocalDateTime createAt;

    private UUID id;

    private String name;

    private String text;

    private LocalDateTime updateAt;

    private Integer version;

}
