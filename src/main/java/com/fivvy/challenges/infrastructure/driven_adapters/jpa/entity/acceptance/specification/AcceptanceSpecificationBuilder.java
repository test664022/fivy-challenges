package com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.acceptance.specification;

import com.fivvy.challenges.domain.model.page.Pageable;
import com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.acceptance.AcceptanceEntity;
import com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.acceptance.filter.AcceptanceFilter;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.data.jpa.domain.Specification;

public abstract class AcceptanceSpecificationBuilder {

    public AcceptanceFilter getFilter(Pageable pageable) {

        var filter = new AcceptanceFilter();

        if (pageable.getQuery() == null ) {
            return filter;
        }

        var matcher = getMatcher(
            pageable.getQuery().replace("'", "").replace("*", "")
        );

        while (matcher.find()) {

            var key = matcher.group(1);
            var value = matcher.group(2);

            if ("userId".equals(key)) {
                filter.setUserId(UUID.fromString(value));
            }
        }

        return filter;
    }

    private Matcher getMatcher(String search) {

        var pattern = Pattern.compile("([a-zA-Z_]*):([a-zA-Z0-9-:/_]*)");

        return pattern.matcher(search);
    }

    public Specification<AcceptanceEntity> containsUserId(AcceptanceFilter filter) {

        return (root, query, criteriaBuilder) -> {

            if (filter.getUserId() == null) {
                return null;
            }

            return criteriaBuilder.equal(root.get("userId"), filter.getUserId());
        };
    }

}
