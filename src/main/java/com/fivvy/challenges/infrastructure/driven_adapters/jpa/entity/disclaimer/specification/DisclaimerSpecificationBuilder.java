package com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.disclaimer.specification;

import com.fivvy.challenges.domain.model.page.Pageable;
import com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.disclaimer.DisclaimerEntity;
import com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.disclaimer.filter.DisclaimerFilter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.data.jpa.domain.Specification;

public abstract class DisclaimerSpecificationBuilder {

    public DisclaimerFilter getFilter(Pageable pageable) {

        var filter = new DisclaimerFilter();

        if (pageable.getQuery() == null ) {
            return filter;
        }

        var matcher = getMatcher(
            pageable.getQuery().replace("'", "").replace("*", "")
        );

        while (matcher.find()) {

            var key = matcher.group(1);
            var value = matcher.group(2);

            if ("text".equals(key)) {
                filter.setText(value);
            }
        }

        return filter;
    }

    private Matcher getMatcher(String search) {

        var pattern = Pattern.compile("([a-zA-Z_]*):([a-zA-Z0-9-:/_]*)");

        return pattern.matcher(search);
    }

    public Specification<DisclaimerEntity> containsText(DisclaimerFilter filter) {

        return (root, query, criteriaBuilder) -> {

            if (filter.getText() == null) {
                return null;
            }

            return criteriaBuilder.like(root.get("text"), "%" + filter.getText() + "%");
        };
    }

}
