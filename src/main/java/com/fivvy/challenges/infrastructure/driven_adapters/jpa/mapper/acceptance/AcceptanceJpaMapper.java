package com.fivvy.challenges.infrastructure.driven_adapters.jpa.mapper.acceptance;

import com.fivvy.challenges.domain.model.acceptance.Acceptance;
import com.fivvy.challenges.domain.model.page.Page;
import com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.acceptance.AcceptanceEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(
    componentModel = "spring",
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface AcceptanceJpaMapper {

    AcceptanceEntity toEntity(Acceptance domain);

    Acceptance toDomain(AcceptanceEntity entity);

    @Mapping(target = "currentPage", expression = "java( page.getPageable().getPageNumber() )")
    @Mapping(target = "content", defaultExpression = ("java( new ArrayList<Acceptance>() )"))
    Page<Acceptance> toDomainPage(
        org.springframework.data.domain.Page<AcceptanceEntity> page
    );

}
