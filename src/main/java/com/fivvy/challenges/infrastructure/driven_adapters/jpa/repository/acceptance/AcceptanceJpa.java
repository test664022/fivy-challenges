package com.fivvy.challenges.infrastructure.driven_adapters.jpa.repository.acceptance;

import com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.acceptance.AcceptanceEntity;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AcceptanceJpa extends
    JpaRepository<AcceptanceEntity, UUID>,
    JpaSpecificationExecutor<AcceptanceEntity> {

}
