package com.fivvy.challenges.infrastructure.driven_adapters.jpa.repository.disclaimer;

import com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.disclaimer.DisclaimerEntity;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DisclaimerJpa extends
    JpaRepository<DisclaimerEntity, UUID>,
    JpaSpecificationExecutor<DisclaimerEntity> {
}
