package com.fivvy.challenges.infrastructure.driven_adapters.jpa.mapper.disclaimer;

import com.fivvy.challenges.domain.model.disclaimer.Disclaimer;
import com.fivvy.challenges.domain.model.page.Page;
import com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.disclaimer.DisclaimerEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(
    componentModel = "spring",
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface DisclaimerJpaMapper {

    Disclaimer toDomain(DisclaimerEntity entity);

    DisclaimerEntity toEntity(Disclaimer domain);

    void updateEntity(Disclaimer domain, @MappingTarget DisclaimerEntity entity);

    @Mapping(target = "currentPage", expression = "java( page.getPageable().getPageNumber() )")
    @Mapping(target = "content", defaultExpression = ("java( new ArrayList<Disclaimer>() )"))
    Page<Disclaimer> toDomainPage(
        org.springframework.data.domain.Page<DisclaimerEntity> page
    );
}
