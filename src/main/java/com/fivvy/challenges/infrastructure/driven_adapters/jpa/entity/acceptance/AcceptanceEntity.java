package com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.acceptance;

import com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.disclaimer.DisclaimerEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

@Data
@Entity
@SQLDelete(sql = "UPDATE acceptances SET is_deleted = true, delete_at=now() WHERE id=?")
@Table(name = "acceptances")
@Where(clause = "is_deleted is false")
public class AcceptanceEntity {

    @CreationTimestamp
    private LocalDateTime createAt;

    private LocalDateTime deleteAt;

    @ManyToOne(fetch = FetchType.LAZY)
    private DisclaimerEntity disclaimer;

    @Id
    @Column(updatable = false, nullable = false)
    private UUID id = UUID.randomUUID();

    private Boolean isDeleted = false;

    @UpdateTimestamp
    private LocalDateTime updateAt;

    @Column(nullable = false)
    private UUID userId;

}
