package com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.disclaimer.filter;

import lombok.Data;

@Data
public class DisclaimerFilter {

    private String text;
}
