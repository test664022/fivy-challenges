package com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.disclaimer;

import com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.acceptance.AcceptanceEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

@Data
@Entity
@SQLDelete(sql = "UPDATE disclaimers SET is_deleted = true, delete_at=now() WHERE id=?")
@Table(name = "disclaimers")
@Where(clause = "is_deleted is false")
public class DisclaimerEntity {

    @CreationTimestamp
    private LocalDateTime createAt;

    private LocalDateTime deleteAt;

    @Id
    @Column(updatable = false, nullable = false)
    private UUID id = UUID.randomUUID();

    private Boolean isDeleted = false;

    @Column(nullable = false, unique = true)
    private String name;

    private String text;

    @UpdateTimestamp
    private LocalDateTime updateAt;

    private Integer version = 0;

    @OneToMany(mappedBy = "disclaimer")
    private List<AcceptanceEntity> acceptances;

}
