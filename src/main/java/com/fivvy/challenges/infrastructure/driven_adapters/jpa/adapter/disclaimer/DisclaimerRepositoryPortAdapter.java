package com.fivvy.challenges.infrastructure.driven_adapters.jpa.adapter.disclaimer;

import com.fivvy.challenges.domain.model.disclaimer.Disclaimer;
import com.fivvy.challenges.domain.model.disclaimer.driven_port.DisclaimerRepositoryPort;
import com.fivvy.challenges.domain.model.page.Page;
import com.fivvy.challenges.domain.model.page.Pageable;
import com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.disclaimer.DisclaimerEntity;
import com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.disclaimer.specification.DisclaimerSpecificationBuilder;
import com.fivvy.challenges.infrastructure.driven_adapters.jpa.mapper.disclaimer.DisclaimerJpaMapper;
import com.fivvy.challenges.infrastructure.driven_adapters.jpa.repository.disclaimer.DisclaimerJpa;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class DisclaimerRepositoryPortAdapter extends DisclaimerSpecificationBuilder
    implements DisclaimerRepositoryPort {

    private DisclaimerJpaMapper disclaimerJpaMapper;
    private DisclaimerJpa disclaimerJpa;

    @Override
    public Page<Disclaimer> findAll(Pageable pageable) {

        var sortCreatedAt = "createAt";

        var pageRequest = PageRequest.of(
            pageable.getPage(), pageable.getSize(), Sort.by(sortCreatedAt).descending()
        );

        var filter = getFilter(pageable);

        var disclaimerPage = disclaimerJpa.findAll(containsText(filter), pageRequest);

        return disclaimerJpaMapper.toDomainPage(disclaimerPage);
    }

    @Override
    public Disclaimer findById(UUID disclaimerId) {

        return disclaimerJpaMapper.toDomain(
            findByUuid(disclaimerId)
        );
    }

    @Override
    public Disclaimer save(Disclaimer disclaimer) {

        var disclaimerEntity = disclaimerJpa.save(
            disclaimerJpaMapper.toEntity(disclaimer)
        );

        return disclaimerJpaMapper.toDomain(disclaimerEntity);
    }

    @Override
    public Disclaimer update(UUID disclaimerId, Disclaimer disclaimer) {

        var disclaimerEntityFound = findByUuid(disclaimerId);

        disclaimerJpaMapper.updateEntity(disclaimer, disclaimerEntityFound);

        return disclaimerJpaMapper.toDomain(
            disclaimerJpa.save(disclaimerEntityFound)
        );
    }

    @Override
    public void delete(UUID disclaimerId) {

        var disclaimerEntityFound = findByUuid(disclaimerId);

        disclaimerJpa.delete(disclaimerEntityFound);
    }

    private DisclaimerEntity findByUuid(UUID disclaimerUuid) {

        return disclaimerJpa.findById(disclaimerUuid)
            .orElseThrow( () -> new IllegalArgumentException("Disclaimer not found"));
    }

}
