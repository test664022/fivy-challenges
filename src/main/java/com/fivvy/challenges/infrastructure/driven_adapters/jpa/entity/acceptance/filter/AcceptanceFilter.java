package com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.acceptance.filter;

import java.util.UUID;
import lombok.Data;

@Data
public class AcceptanceFilter {

    private UUID userId;
}
