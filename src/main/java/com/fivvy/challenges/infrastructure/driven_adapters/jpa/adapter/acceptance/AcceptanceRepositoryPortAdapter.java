package com.fivvy.challenges.infrastructure.driven_adapters.jpa.adapter.acceptance;

import com.fivvy.challenges.domain.model.acceptance.Acceptance;
import com.fivvy.challenges.domain.model.acceptance.driven_port.AcceptanceRepositoryPort;
import com.fivvy.challenges.domain.model.page.Page;
import com.fivvy.challenges.domain.model.page.Pageable;
import com.fivvy.challenges.infrastructure.driven_adapters.jpa.entity.acceptance.specification.AcceptanceSpecificationBuilder;
import com.fivvy.challenges.infrastructure.driven_adapters.jpa.mapper.acceptance.AcceptanceJpaMapper;
import com.fivvy.challenges.infrastructure.driven_adapters.jpa.repository.acceptance.AcceptanceJpa;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class AcceptanceRepositoryPortAdapter extends AcceptanceSpecificationBuilder
    implements AcceptanceRepositoryPort {

    private AcceptanceJpa acceptanceJpa;
    private AcceptanceJpaMapper acceptanceJpaMapper;

    @Override
    public Page<Acceptance> findAll(Pageable pageable) {

        var sortCreatedAt = "createAt";

        var pageRequest = PageRequest.of(
            pageable.getPage(), pageable.getSize(), Sort.by(sortCreatedAt).descending()
        );

        var filter = getFilter(pageable);

        var acceptancePage = acceptanceJpa.findAll(containsUserId(filter), pageRequest);

        return acceptanceJpaMapper.toDomainPage(acceptancePage);
    }

    @Override
    public Acceptance save(Acceptance acceptance) {

        var acceptanceEntity = acceptanceJpa.save(
            acceptanceJpaMapper.toEntity(acceptance)
        );

        return acceptanceJpaMapper.toDomain(acceptanceEntity);
    }
}
