package com.fivvy.challenges.application.use_case.disclaimer;

import com.fivvy.challenges.domain.model.disclaimer.Disclaimer;
import com.fivvy.challenges.domain.model.disclaimer.driven_port.DisclaimerRepositoryPort;
import com.fivvy.challenges.domain.model.page.Page;
import com.fivvy.challenges.domain.model.page.Pageable;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class FindAllDisclaimerUseCase {

    private DisclaimerRepositoryPort disclaimerRepositoryPort;

    public Page<Disclaimer> execute(Pageable pageable) {

        return disclaimerRepositoryPort.findAll(pageable);
    }

}
