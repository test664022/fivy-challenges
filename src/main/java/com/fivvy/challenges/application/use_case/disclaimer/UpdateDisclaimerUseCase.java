package com.fivvy.challenges.application.use_case.disclaimer;

import com.fivvy.challenges.domain.model.disclaimer.Disclaimer;
import com.fivvy.challenges.domain.model.disclaimer.driven_port.DisclaimerRepositoryPort;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class UpdateDisclaimerUseCase {

    private DisclaimerRepositoryPort disclaimerRepositoryPort;

    public Disclaimer execute(UUID disclaimerId, Disclaimer disclaimer) {

        return disclaimerRepositoryPort.update(disclaimerId, disclaimer);
    }

}
