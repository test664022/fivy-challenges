package com.fivvy.challenges.application.use_case.acceptance;

import com.fivvy.challenges.domain.model.acceptance.Acceptance;
import com.fivvy.challenges.domain.model.acceptance.driven_port.AcceptanceRepositoryPort;
import com.fivvy.challenges.domain.model.page.Page;
import com.fivvy.challenges.domain.model.page.Pageable;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class FindAllAcceptanceUseCase {

    private AcceptanceRepositoryPort acceptanceRepositoryPort;

    public Page<Acceptance> execute(Pageable pageable) {

        return acceptanceRepositoryPort.findAll(pageable);
    }
}
