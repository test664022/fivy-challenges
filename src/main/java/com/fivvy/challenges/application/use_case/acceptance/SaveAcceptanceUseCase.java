package com.fivvy.challenges.application.use_case.acceptance;

import com.fivvy.challenges.domain.model.acceptance.Acceptance;
import com.fivvy.challenges.domain.model.acceptance.driven_port.AcceptanceRepositoryPort;
import com.fivvy.challenges.domain.model.disclaimer.driven_port.DisclaimerRepositoryPort;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class SaveAcceptanceUseCase {

    private AcceptanceRepositoryPort acceptanceRepositoryPort;
    private DisclaimerRepositoryPort disclaimerRepositoryPort;

    public Acceptance execute(Acceptance acceptance) {

        var disclaimer = disclaimerRepositoryPort.findById(acceptance.getDisclaimer().getId());

        acceptance.setDisclaimer(disclaimer);

        return acceptanceRepositoryPort.save(acceptance);
    }

}
