package com.fivvy.challenges.application.use_case.disclaimer;

import com.fivvy.challenges.domain.model.disclaimer.driven_port.DisclaimerRepositoryPort;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class DeleteDisclaimerUseCase {

    private DisclaimerRepositoryPort disclaimerRepositoryPort;

    public void execute(UUID disclaimerId) {

        disclaimerRepositoryPort.delete(disclaimerId);
    }

}
