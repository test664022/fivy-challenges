package com.fivvy.challenges.application.use_case.disclaimer;

import com.fivvy.challenges.domain.model.disclaimer.Disclaimer;
import com.fivvy.challenges.domain.model.disclaimer.driven_port.DisclaimerRepositoryPort;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class SaveDisclaimerUseCase {

    private DisclaimerRepositoryPort disclaimerRepositoryPort;

    public Disclaimer execute(Disclaimer disclaimer) {

        return disclaimerRepositoryPort.save(disclaimer);
    }

}
