# challenges


## Getting started

This project was created with a basic approach to hexagonal architecture. It should be noted that an error handler was not taken into account, it can be left for a next iteration.

To test the operation of the microservice follow these steps:

### Clone the project

```
For example:
git clone https://gitlab.com/test664022/fivy-challenges.git
```
Go to the root folder of the project and execute in terminal:
```
./gradlew clean build
```

### Run docker-compose
```
docker-compose up --build
```

### Run the following endpoints
#### Disclaimer entity
```
POST: (save disclaimers)
localhost:8096/api/v1/disclaimers

Request body example:
{
    "name": "First disclaimer",
    "text": "Text to first disclaimer",
    "version": 1
}
```

```
GET: (find all disclaimers)
localhost:8096/api/v1/disclaimers

If you want to filter by the text field:
localhost:8096/api/v1/disclaimers?search=text:first
```

```
GET: (find a disclaimer)
localhost:8096/api/v1/disclaimers/{disclaimer_id}
```

```
PUT: (update a disclaimer)
localhost:8096/api/v1/disclaimers/{disclaimer_id}

Request body example (one can be sent to all these fields):
{
    "name": "",
    "text": "",
    "version": 1
}
```

```
DELETE: (delete a disclaimer)
localhost:8096/api/v1/disclaimers/{disclaimer_id}
```

#### Acceptance entity
```
POST: (save acceptance)
localhost:8096/api/v1/acceptances

Request body example:
{
    "disclaimer_id": "{disclaimer_id}",
    "user_id": "67dc6a55-d447-4c96-a492-830efe74e4f0"
}
The value of the user_id field can be this or any other that has the UUID format for example:

4561ec4a-3199-4fae-aa1d-1297a875b69b
a1a55d21-98c9-45a7-a187-329bacc92268
c5b55066-37b1-4750-a65e-8fd2d46af0f3
2ee9b493-6b5e-4d3a-be43-e74763a58889
d9f96d5f-b554-4cbe-97f3-7bb66318fda1
bd00e912-5c8f-4822-af64-3ca0e7622eef
c2acfb36-cb9e-4960-9bcc-faaa27914cfa
9e38d98e-15a8-4c06-9091-d801df3beb26
```

```
GET: (find all acceptances)
localhost:8096/api/v1/acceptances

If you want to filter by the user_id field:
localhost:8096/api/v1/acceptances?search=userId:{user_id}
```